const path = require("path");
const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const jwt = require("jsonwebtoken");
const passport = require("passport");
const bodyParser = require("body-parser");
const users = require("./users.json");
const texts = require("./texts.json");

require("./passport.config");

server.listen(3000);

app.use(express.static(path.join(__dirname, "public")));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.get(
  "/game",
  /*passport.authenticate('jwt',{session:false}),*/ function(req, res) {
    res.sendFile(path.join(__dirname, "game.html"));
  }
);

app.get("/text", passport.authenticate("jwt", { session: false }), function(
  req,
  res
) {
  let textsLength = texts.length;
  let text = texts[Math.floor(Math.random() * textsLength)];
  res.send(text.text);
});

app.get("/login", function(req, res) {
  res.sendFile(path.join(__dirname, "login.html"));
});

app.post("/login", function(req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, "someSecret", { expiresIn: "24h" });
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

const TIME_TO_REGISTRATION = 10;
const GAME_TIME = 100;
let connected = 0;
let conectedUsers = [];
let timeToRegistration = TIME_TO_REGISTRATION;
let gameTime = GAME_TIME;

let botTimeline = [
  {
    stage: "registration",
    time: 1,
    phrase: "Ну что же, начинаем"
  },
  {
    stage: "registration",
    time: Math.floor(TIME_TO_REGISTRATION * 0.5),
    phrase: `Половина регистрации на гонку уже позади`
  },
  {
    stage: "registration",
    time: TIME_TO_REGISTRATION,
    phrase: "Всем привет"
  },
  {
    time: 2,
    stage: "game",
    phrase: "Вот и закнчилась наша гонка. Было интересно, не так ли?"
  },
  {
    time: Math.floor(GAME_TIME * 0.3),
    stage: "game",
    phrase: "Гонка подходит к концу"
  },
  {
    time: Math.floor(GAME_TIME * 0.5),
    stage: "game",
    phrase: "Уже половина"
  },
  {
    time: Math.floor(GAME_TIME * 0.7),
    stage: "game",
    phrase: "Треть"
  },
  {
    time: GAME_TIME,
    stage: "game",
    phrase: "Гонка началась. Ура"
  }
];

io.on("connection", socket => {
  connected = 0;
  socket.on("userLogs", payload => {
    const { token } = payload;
    const userLogin = jwt.decode(token).login;
    if (timeToRegistration === TIME_TO_REGISTRATION) {
      let startGameTimer = setInterval(function() {
        socket.emit("registration", timeToRegistration);
        socket.broadcast.emit("registration", timeToRegistration);
        let botPhraseOnRegistr = botTimeline.filter(phrase => {
          return (
            phrase.stage == "registration" && phrase.time == timeToRegistration
          );
        });
        if (botPhraseOnRegistr.length > 0) {
          console.log(botPhraseOnRegistr[0]);
          socket.emit("botCommentOnRegistr", { botPhraseOnRegistr, userLogin });
          socket.broadcast.emit("botCommentOnRegistr", {
            botPhraseOnRegistr,
            userLogin
          });
        }
        timeToRegistration--;

        if (timeToRegistration < 0) {
          botTimeline.push({
            stage: "game",
            time: GAME_TIME - 1,
            players: conectedUsers.length,
            phrase: `В гонке примет участие ${
              conectedUsers.length
            } человек. Это ${conectedUsers
              .map(player => {
                return player.login;
              })
              .join(", ")}`
          });
          clearInterval(startGameTimer);
          timeToRegistration = TIME_TO_REGISTRATION;
        }
      }, 1000);
    }
    socket.on("gameStart", () => {
      if (connected == 0) {
        let gameTimer = setInterval(function() {
          socket.emit("game", gameTime);
          socket.broadcast.emit("game", gameTime);
          let botPhraseOnGame = botTimeline.filter(phrase => {
            return phrase.stage == "game" && phrase.time == gameTime;
          });
          if (botPhraseOnGame.length > 0) {
            console.log(botPhraseOnGame[0]);
            socket.emit("botCommentOnGame", {
              botPhraseOnGame,
              userLogin
            });
            socket.broadcast.emit("botCommentOnGame", {
              botPhraseOnGame,
              userLogin
            });
          }
          gameTime--;
          if (gameTime < 0) {
            clearInterval(gameTimer);
            gameTime = GAME_TIME;
          }
        }, 1000);
      }
      connected++;
    });
    if (timeToRegistration <= TIME_TO_REGISTRATION || gameTime == GAME_TIME) {
      conectedUsers.push({
        login: userLogin,
        token: token,
        connect: "connected"
      });
      socket.broadcast.emit("newUser", { user: userLogin, conectedUsers });
      socket.emit("newUser", { user: userLogin, conectedUsers });
    }

    socket.on("userProgress", payload => {
      let userToken = payload.token;
      const userLogin = jwt.decode(payload.token).login;
      let progress = payload.progressPercentage;
      socket.broadcast.emit("playerProgress", {
        userToken,
        progress,
        userLogin
      });
      socket.emit("playerProgress", { userToken, progress, userLogin });
    });
    socket.on("winner", payload => {
      let winner = payload.user;
      botTimeline.push({
        stage: "game",
        time: 0,
        phrase: `Эту гонку выиграл ${winner}`
      });
      socket.emit("trackWinner", { winner });
      socket.broadcast.emit("trackWinner", { winner });
    });
    socket.on("disconnect", function() {
      connected--;
      console.log("Got disconnect!");
      const disconnectedUserToken = token;
      conectedUsers = conectedUsers.filter(function(obj) {
        return obj.token !== disconnectedUserToken;
      });
      socket.emit("userDisconected", {
        user: userLogin,
        token: token,
        conectedUsers,
        connect: "disconected"
      });
      socket.broadcast.emit("userDisconected", {
        user: userLogin,
        token: token,
        conectedUsers,
        connect: "disconected"
      });
    });
  });
});
