window.onload = () => {
  let jwt = localStorage.getItem("jwt");
  let track = document.querySelector("#track");
  let timeElem = document.querySelector(".time");
  let userListElem = document.querySelector("#users-list");
  let userCount = document.querySelector(".users-count");
  let botComments = document.querySelector(".comment");
  let startGameTimer = document.querySelector("#game-start-timer");

  const wrapSubstring = (sourceString, startIndex, endIndex) => {
    return (
      sourceString.substring(0, startIndex) +
      "<span>" +
      sourceString.substring(startIndex, endIndex) +
      "</span>" +
      (endIndex ? sourceString.substring(endIndex) : "")
    );
  };

  const arrayUnique = arr => {
    return arr.reduce((unique, o) => {
      if (!unique.some(obj => obj.login === o.login && obj.token === o.token)) {
        unique.push(o);
      }
      return unique;
    }, []);
  };

  const renderText = (element, text) => (element.innerText = text);

  const getTrackText = (route, token) => {
    fetch(route, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(response => {
        let res = response.text();
        return res;
      })
      .then(text => {
        renderText(track, text);
      })
      .catch(err => {
        console.log("request went wrong");
      });
  };

  if (!jwt) {
    location.replace("/login");
  } else {
    const socket = io.connect("http://localhost:3000");
    socket.emit("userLogs", { token: jwt });

    socket.on("newUser", payload => {
      socket.on("registration", timeToRegistration => {
        startGameTimer.innerHTML = timeToRegistration;
        if (timeToRegistration == 0) {
          getTrackText("/text", jwt);
          track.classList.remove("hidden");
          socket.emit("gameStart");
          socket.on("game", gameTime => {
            renderText(
              timeElem,
              `Игра началась. До конца осталось ${gameTime} секунд`
            );
          });
        }
      });
      socket.on("botCommentOnRegistr", payload => {
        renderText(botComments, payload.botPhraseOnRegistr[0].phrase);
      });
      socket.on("botCommentOnGame", payload => {
        renderText(botComments, payload.botPhraseOnGame[0].phrase);
      });

      let sockets = payload.conectedUsers;
      let uniqueUsers = arrayUnique(sockets);
      renderText(userCount, uniqueUsers.length);
      renderText(userListElem, "");
      uniqueUsers.forEach(user => {
        let newLi = document.createElement("li");
        let progressBar = document.createElement("div");
        let progress = document.createElement("div");
        progressBar.appendChild(progress);
        //let left = document.createElement("div");
        newLi.dataset.tokenId = user.token;
        newLi.dataset.connect = user.connect;
        newLi.innerHTML = `${user.login}`;
        progressBar.classList.add("progress");
        progress.dataset.tokenId = user.token;
        newLi.appendChild(progressBar);
        userListElem.appendChild(newLi);
      });
    });
    socket.on("userDisconected", payload => {
      let userElem = document.querySelectorAll("#users-list li");
      let sockets = payload.conectedUsers;
      var uniqueUsers = arrayUnique(sockets);
      userCount.innerHTML = `${uniqueUsers.length}`;
      userElem.forEach(e => {
        if (e.dataset.tokenId == payload.token) {
          e.dataset.connect = "disconected";
        }
      });
    });

    let charCount = 0;
    document.addEventListener("keypress", e => {
      let user = "";
      let trackTextElem = document.querySelector("#track");
      let trackText = document.querySelector("#track").textContent.toString();
      let trackLength = trackText.length;
      let keynum = e.keyCode;
      let pressedChar = String.fromCharCode(keynum);
      let progressBars = document.querySelectorAll(".progress");
      if (pressedChar == trackText.charAt(charCount)) {
        charCount += 1;
        let progressPercentage = Math.round((charCount / trackLength) * 100);
        socket.emit("userProgress", { progressPercentage, token: jwt });
        socket.on("playerProgress", payload => {
          console.log(payload.userToken);
          progressBars.forEach(bar => {
            if (bar.firstChild.dataset.tokenId == payload.userToken) {
              bar.firstChild.style.width = `${payload.progress}%`;
            }
          });
        });
        let text = wrapSubstring(trackText, 0, charCount);
        trackTextElem.innerHTML = text;
        if (charCount >= trackLength) {
          socket.on("playerProgress", payload => {
            user = payload.userLogin;
            socket.emit("winner", { user });
          });
        }
      }
    });
    socket.on("trackWinner", payload => {
      let winner = payload.winner;
      alert(`Молодец, ${winner}! Ты победил.`);
      renderText(botComments, `Вот это победа! ${winner} молодец!`);
    });
  }
};
